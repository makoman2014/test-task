import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from './reducers';
import { composeWithDevTools } from 'redux-devtools-extension';

const loggerMiddleware = createLogger();

const middleware = composeWithDevTools(
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware
  )
)
export const store = createStore(
    rootReducer,
    middleware
);
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
