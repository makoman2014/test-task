import { combineReducers } from 'redux';
import * as treeReducer from './tree.reducer';


const rootReducer = combineReducers({
      ...treeReducer,
});

export default rootReducer;
