import {
  FAIL_ACTION_TREE, LOADING_ACTION_TREE, SUCCESS_ACTION_TREE, SUCCESS_GET_TREE
} from '../actionTypes';
import {MenuInterface} from "../../interfaces/menuInterface";

export type errorType ={
  errors:string
  loading:boolean
}

const initialState = {
  data: [],
  errors: ''
}

function treeReducer(state = initialState, action: any):MenuInterface {
  switch (action.type) {
    case SUCCESS_GET_TREE:
      return {
        data: [{...action.data, name: "Root"}],
        rootId: action.data.id,
        errors: ''
      };
    default:
      return state
  }
}
function treeReducerPopup(state:errorType = {errors: '', loading: false}, action: any):errorType {
  switch (action.type) {
    case LOADING_ACTION_TREE:
      return {
        errors: "",
        loading: true
      };
    case SUCCESS_ACTION_TREE:
      return {
        errors: action.errors,
        loading: false
      };
     case FAIL_ACTION_TREE:
      return {
        errors: action.errors,
        loading: false
      };
    default:
      return state
  }
}


export {treeReducer, treeReducerPopup}
