import qs from "qs";

let generateGUID = ():string => {
    let guid = localStorage.getItem("GUID")
    if(guid){
        return guid
    }
    let uniqueId = Date.now().toString(36) + Math.random().toString(36).substring(2);
    localStorage.setItem("GUID", uniqueId)
    return uniqueId
}

export function PostInfo(url:string, queryParams:any ) {
    url += `?treeName=${generateGUID()}&${queryParams && qs.stringify(queryParams)}`
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: ''
    };
    return fetch(`${url}`, requestOptions).then(handleResponse)
}
function handleResponse(response:any) {
    return response.text().then((text:string) => {
        let data = text && (typeof text === 'string') && JSON.parse(text)
        if(response.ok){
            return Promise.resolve(data);
        }
        else  {
            return Promise.reject(data);
        }
    });
}