import {AppDispatch} from '../store';
import {
    FAIL_ACTION_TREE,
    LOADING_ACTION_TREE,
    SUCCESS_GET_TREE,
    SUCCESS_ACTION_TREE
} from '../actionTypes';
import {PostInfo} from "../services/services";

type dataType = {
    parentNodeId?:number
    nodeName?:string
    nodeId?:number
    newNodeName?:string
}

const getTree =
  () =>
    async (dispatch: AppDispatch): Promise<void> => {
      try {
        const res = await PostInfo('/api.user.tree.get', {})
          if (res) {
          dispatch({type: SUCCESS_GET_TREE, data: res, loading: false, errors: ''});
        }
      } catch (err) {
          console.log(err)
      }
    };

const addEditTree =
  (data:dataType, api: string) =>
    async (dispatch: AppDispatch): Promise<void> => {
      dispatch({type: LOADING_ACTION_TREE,  errors: ''});
      try {
        await PostInfo(api, data)
        dispatch({type: SUCCESS_ACTION_TREE,  errors: ''});
        dispatch(treeActions.getTree())
      } catch (err:any) {
          dispatch({type: FAIL_ACTION_TREE, errors: err.data.message});
          return Promise.reject();
      }
    };


export const treeActions = {
    getTree,
    addEditTree
};
