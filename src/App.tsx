import React from 'react';
import {TreePanel} from "./components/Tree/TreePanel";
import { Provider } from 'react-redux';
import { store } from './redux/store';

function App() {
  return (
    <div className="App">
        <Provider store={store}>
            <TreePanel/>
        </Provider>
    </div>
  );
}

export default App;
