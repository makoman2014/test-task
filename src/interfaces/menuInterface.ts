export interface TreeInterface {
  menuItems:
    {
      name: string
      id: number
      children?: any
    }[]
    rootId?: number
}
export interface MenuInterface {
    data: {
        name: string
        id: number
        children?: any
    }[],
    rootId?: number
    errors?: string
    loading?: boolean

}