import styled from "styled-components";

export const StyledTreeElement = styled.div`
    color: #5E6366;
    list-style: none;
    margin-left: 15px;
    margin-bottom: 5px;
 
    .menu-row{
      height: 30px;
      display: flex;
      align-items: center;
      &:hover .actions{
        display: flex;
      }
      .arrow_icon{
        cursor: pointer;
        width: 16px;
        display: flex;
        align-items: center;
        transform: rotate(-90deg);
        transition: all .3s;
        margin: 0 5px;
        &.active{
          transform: rotate(0);
        }
      }
    }
  
  .actions{
    display: none;
    align-items: center;
    button{
      background: transparent;
      margin-left: 5px;
      border: none;
      cursor: pointer;
    }
  }

`;