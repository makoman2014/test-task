import React, {useEffect} from "react";
import {TreeElements} from "./TreeElements";
import {treeActions} from "../../redux/actions/tree.actions";
import {useAppDispatch, useAppSelector} from "../../redux";
import {MenuInterface} from "../../interfaces/menuInterface";

export  const TreePanel = () => {
    const dispatch = useAppDispatch();
    const treeData = useAppSelector<MenuInterface>(
        (state) => state.treeReducer
    );
    useEffect(()=> {
        dispatch(treeActions.getTree())
    },[dispatch])
    return (
        <>
            <TreeElements menuItems={treeData.data} rootId={treeData.rootId}/>
        </>

    )
}