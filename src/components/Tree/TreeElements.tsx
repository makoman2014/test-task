import React, {useState} from "react";
import {TreeInterface} from "../../interfaces/menuInterface";
import {StyledTreeElement} from "./TreeElements.styled";
import AddCircleIcon from '@mui/icons-material/AddCircle';
import EditIcon from '@mui/icons-material/Edit';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import TreePopup from "../Popups/TreePopup";
interface menuOpenInterFace {
    [key: string]: number;
}
type DialogType = {
    title: string,
    type: string,
    name?: string,
    id?: number | undefined,
}
export const TreeElements = ({menuItems, rootId}:TreeInterface) => {
    const [openMenu, setOpenMenu] = useState<menuOpenInterFace>({})
    const [openDialog, setOpenDialog] = useState<boolean>(false)
    const [modalInfo, setModalInfo] = useState<DialogType>();
    const openPopup = (title:string, type:string, name?:string, id?:number) =>{
        setOpenDialog(true)
        setModalInfo({title, type, name, id})
    }
    return (
        <>
            {menuItems && menuItems.map(menu=>
                <StyledTreeElement key={menu.id}>
                    <div className="menu-row">
                        {menu?.children?.length > 0 ?
                            <span
                                onClick={():void => setOpenMenu({...openMenu, [menu.id]: !openMenu[menu.id]})}
                                className={`arrow_icon ${openMenu[menu.id] ? 'active' : ''}`}
                            >
                                <svg width="16" height="24" viewBox="0 0 16 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path opacity="0.48" d="M3.41 8L8 12.59L12.59 8L14 9.42L8 15.42L2 9.42L3.41 8Z" fill="#78909C"/>
                                </svg>
                            </span>
                            :
                            <span className="arrow_icon"></span>
                        }
                        <span className="menu_name">{menu.name}</span>
                        <div className="actions">
                            <button
                                onClick={()=>openPopup("Add", "Node Name",  '', menu.id)}
                            >
                                <AddCircleIcon color="primary"/>
                            </button>
                            {rootId !== menu.id &&
                                <>
                                    <button
                                        onClick={()=>openPopup("Rename", "New Node Name",  menu.name, menu.id)}
                                    >
                                        <EditIcon color="primary"/>
                                    </button>
                                    <button
                                        onClick={()=>openPopup("Delete", "",  menu.name, menu.id)}

                                    ><DeleteForeverIcon color="error"/></button>
                                </>
                            }

                        </div>

                    </div>
                    {menu?.children?.length > 0 && openMenu[menu.id] && <TreeElements menuItems={menu.children} />}
                </StyledTreeElement>
            )}
            {openDialog &&
                <TreePopup
                    name={modalInfo?.name}
                    title={modalInfo?.title}
                    type={modalInfo?.type}
                    open={openDialog}
                    id={modalInfo?.id}
                    setOpen={setOpenDialog}
                />
            }

        </>
    )
}