import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import {treeActions} from "../../redux/actions/tree.actions";
import {useCallback, useEffect, useState} from "react";
import {useAppDispatch, useAppSelector} from "../../redux";
import {errorType} from "../../redux/reducers/tree.reducer";
import Spinner from "../../media/Spinner.svg";
import {SUCCESS_ACTION_TREE} from "../../redux/actionTypes";

type DialogType = {
    id?: number | undefined,
    title?: string,
    type?: string,
    name?: string,
    open: boolean,
    setOpen: (x:boolean) => void
}


export default function TreePopup({title, type, name, open, setOpen, id}: DialogType) {
    const [nodeName, setNodeName] = useState<string|undefined>(name)
    const dispatch = useAppDispatch();
    const {errors, loading} = useAppSelector<errorType>(
        (state) => state.treeReducerPopup
    );
    useEffect(()=>{
        setNodeName(name)
    },[name])
    const handleClose = useCallback(() => {
        dispatch({type: SUCCESS_ACTION_TREE,  errors: ''});
        setOpen(false);
    },[dispatch, setOpen]);

    const callTree = useCallback(async () => {
        switch (title) {
            case "Add":
                await dispatch(treeActions.addEditTree({parentNodeId:id, nodeName}, "/api.user.tree.node.create"))
                handleClose()
        }
        switch (title) {
            case "Rename":
                await dispatch(treeActions.addEditTree({nodeId: id, newNodeName: nodeName}, "/api.user.tree.node.rename"))
                handleClose()
        }
        switch (title) {
            case "Delete":
                await dispatch(treeActions.addEditTree({nodeId: id}, "/api.user.tree.node.delete"))
                handleClose()
        }

    }, [id, nodeName, dispatch, handleClose, title])

    return (
        <div>
            <Dialog open={open} onClose={handleClose} fullWidth={true}>
                <DialogTitle>{title}</DialogTitle>
                <DialogContent>
                    {!type ?
                        <DialogContentText>
                            Do you want to delete {nodeName}?
                        </DialogContentText>
                        :
                        <TextField
                            value={nodeName}
                            onChange={e=>setNodeName(e.target.value)}
                            autoFocus
                            margin="dense"
                            id="name"
                            label={type}
                            type="text"
                            fullWidth
                            variant="standard"
                            required
                        />
                    }

                </DialogContent>
                {errors && <div className="errors">{errors}</div>}
                <DialogActions>
                    <Button onClick={handleClose} disabled={loading}>Cancel</Button>
                    <Button onClick={callTree} disabled={(!nodeName && title !== "Delete") || loading}>
                        {title}
                        {loading &&
                            <img src={Spinner} alt="icon" width="35"/>
                        }
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}